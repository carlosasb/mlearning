/** This is a solution to the problem Merchant's Guide to the Galaxy.
 *
 *  This solution uses the keyword "?" to determine if a query is made.
 *  If it is, the program will check to see if a convertion is being asked
 *  or if the value in "credits" of a metal is being asked. To do so, the 
 *  program looks for a sequence of strings (the strings must be adjacent
 *  to one another) and checks for the galactic version of roman numerals
 *  followed by nothing (roman conversion to galactic) or a metal (with its
 *  value in credits known, to be multiplied by the previous value). Invalid
 *  queries are defined as queries in which no string with the previous 
 *  specifications is found.
 *
 *  Valid query exemple  : "how much plik plok pluh iron ?"  (as long as plik, 
 *                          plok and pluh are valid roman numeral equivalents
 *                          and theri ordering is a valid one)
 *  Invalid query exemple: "how much exercise can I do ?"
 *
 *  If the keyword "?" is not found, the string is an attribution 
 *  statement and is considered a definition of the roman to galactic
 *  conversion if it has a valid roman numerical sequence. If no roman numerical 
 *  is found the statement is invalid. If the statemant has information on the 
 *  value of the metals but contains a valid roman numerical sequence, it is valid,
 *  otherwise it is invalid. Note that the actual structure of the statement 
 *  is not important, save for the ordering of the galactic numerals and if an adjacent 
 *  word is a metal recognized by the system.
 *
 *  Valid atribution exemples   : "plik I"
 *                                "plik is I"
 *                                "plok is equal V"
 *                                "plok plik Iron is 39 Credits" 
 *  Invalid atribution exemples : "Iron is 20 Credits"
 *                                "plok plik is 20 Credits"
 *
 *
 *  This is the convert class.
 *
 *  COMPILATION : javac convert.java*/

/* Class convert handles roman to galactic convertions */

import java.util.Iterator;
import java.util.Vector;

class convert
{
    /* roman to number conversion table */
    private String[] roman = new String[7];
    private int[] value = new int[7];

    public convert()
    {
        roman[0] = "I";
        roman[1] = "V";
        roman[2] = "X";
        roman[3] = "L";
        roman[4] = "C";
        roman[5] = "D";
        roman[6] = "M";

        value[0] = 1;
        value[1] = 5;
        value[2] = 10;
        value[3] = 50;
        value[4] = 100;
        value[5] = 500;
        value[6] = 1000;
    }

    public boolean isValidSequence(Vector<Integer> seq)
    {
        boolean isValid;
        int i  = 0;
        int i1 = 0;
        int l  = 0;

        i = 0;
        for (int j = 0; j < seq.size() - 1; j++)
        {
            i = seq.get(j);
            i1 = seq.get(j + 1);
            if (value[i] > value[i + 1])
            {
                l = 0;
            } else if (value[i] == value[i1])
            {
                if (roman[i].equals("I") || roman[i].equals("X") || roman[i].equals("C") || roman[i].equals("M"))
                {
                    l++;
                    if (l > 2)
                    {
                        l = -1;
                        j = seq.size();
                    } 
                 } else
                 {
                     l = -1;
                     j = seq.size();
                 }
            } else if (value[i] < value[i1])
            {
                switch (roman[i])
                {
                    case "I":
                        if (roman[i + 1].equals("V") || roman[i + 1].equals("X"))
                        {
                            l = 0;
                        } else
                        {
                            l = -1;
                            j = seq.size();
                        }
                        break;
                    case "X":
                        if (roman[i1].equals("L") || roman[i1].equals("C"))
                        {
                            l = 0;
                         } else
                        {
                            l = -1;
                            j = seq.size();
                        }
                        break;
                    case "C":
                        if (roman[i1].equals("D") || roman[i1].equals("M"))
                        {
                            l = 0;
                        } else
                        {
                            l = -1;
                            j = seq.size();
                        }
                        break;
                    default:
                        l = -1;
                        j = seq.size();
                        break;
                }
            }
        }

        if (l == -1)
        {
            isValid = false;
        } else
        {
            isValid = true;
        }

        return isValid;
    }

    public double evalSequence(Vector<Integer> seq)
    {
        boolean test;
        double seqVal;
        int i  = 0;
        int i1 = 0;
        int l  = 0;

        for (int j = 1; j < seq.size(); j++)
        {
            i  = seq.get(j);
            i1 = seq.get(j - 1);
            if (value[i1] >= value[i])
            {
                l += value[i1];
            } else if (value[i1] < value[i])
            {
                l += value[i] - value[i1];
                j += 1;
            }
        }

        if (value[i] <= value[i1]) {l += value[i];}

        return seqVal = (double)(l);
    }
}
