/** This is a solution to the problem Merchant's Guide to the Galaxy.
 *
 *  This solution uses the keyword "?" to determine if a query is made.
 *  If it is, the program will check to see if a convertion is being asked
 *  or if the value in "credits" of a metal is being asked. To do so, the 
 *  program looks for a sequence of strings (the strings must be adjacent
 *  to one another) and checks for the galactic version of roman numerals
 *  followed by nothing (roman conversion to galactic) or a metal (with its
 *  value in credits known, to be multiplied by the previous value). Invalid
 *  queries are defined as queries in which no string with the previous 
 *  specifications is found.
 *
 *  Valid query exemple  : "how much plik plok pluh iron ?"  (as long as plik, 
 *                          plok and pluh are valid roman numeral equivalents
 *                          and theri ordering is a valid one)
 *  Invalid query exemple: "how much exercise can I do ?"
 *
 *  If the keyword "?" is not found, the string is an attribution 
 *  statement and is considered a definition of the roman to galactic
 *  conversion if it has a valid roman numerical sequence. If no roman numerical 
 *  is found the statement is invalid. If the statemant has information on the 
 *  value of the metals but contains a valid roman numerical sequence, it is valid,
 *  otherwise it is invalid. Note that the actual structure of the statement 
 *  is not important, save for the ordering of the galactic numerals and if an adjacent 
 *  word is a metal recognized by the system.
 *
 *  Valid atribution exemples   : "plik I"
 *                                "plik is I"
 *                                "plok is equal V"
 *                                "plok plik Iron is 39 Credits" 
 *  Invalid atribution exemples : "Iron is 20 Credits"
 *                                "plok plik is 20 Credits"
 *
 *
 *  This is the main class.*/

/* Class merchants_guide; main class */

import java.util.Iterator;
import java.util.Vector;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class merchants_guide
{
    public static void main(String[] argv) throws FileNotFoundException
    {
        Vector<String> flines = new Vector<String>();
        File input = new File(argv[0]);
        Scanner sc = null;

        try
        {
            sc = new Scanner(input);
            while (sc.hasNext()) 
            {
                String line = sc.nextLine();
                flines.add(line);
            }
            statement queries = new statement(flines);
            queries.evalQuery();
        } finally
        {
            if (sc != null)
            {
                sc.close();
            }
        }
    }
}
