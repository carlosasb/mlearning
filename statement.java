/**  This is the statement class.
  *
  *  COMPILATION: javac statement.java*/

/* Class statement handles the queries and atributions */

import java.util.Iterator;
import java.util.Vector;

class statement
{
    /* galactic numeral sequence */
    String seq;
    /* query result */
    double result;
    /* convert object */
    private convert conv = new convert();
    /* list of atributions and queries made; atribution list is 
     * sorted as roman to galactic definitions before metal rates*/
    private Vector<String> atribution = new Vector<String>();
    private Vector<String> queries = new Vector<String>();
    /* list of galactic to roman numerals */
    private String[] galRoman = new String[7];
    /* list of metal trading rates */
    private String[] validMetalList = new String[12];
    private Vector<String> metals = new Vector<String>();
    private Vector<String> rates = new Vector<String>();
    /* list of query answers, sorted in the order of the queries 
     * made */
    private Vector<String> answers = new Vector<String>();

    public statement(Vector<String> str)
    {
        validMetalList[0]  = "copper";
        validMetalList[1]  = "aluminium";
        validMetalList[2]  = "iron";
        validMetalList[3]  = "gold";
        validMetalList[4]  = "lead";
        validMetalList[5]  = "silver";
        validMetalList[6]  = "mercury";
        validMetalList[7]  = "manganese";
        validMetalList[8]  = "magnesium";
        validMetalList[9]  = "nickel";
        validMetalList[10] = "platinum";
        validMetalList[11] = "zinc";

        for (int i = 0; i < str.size(); i++)
        {
            if (!isQuery(str.get(i)))
            {
                atribution.add(str.get(i));
            } else
            {
                queries.add(str.get(i));
            }
        }

        sortAtr();
        createDef();
    }

    private boolean isQuery(String str)
    {
        String[] token = str.split("\\s+");
        boolean is_query;

        if (token[token.length - 1].equals("?"))
        {
            is_query = true;
        } else
        {
            is_query = false;
        }

        return is_query;
    }

    private boolean isValidQuery(String str)
    {
        String[] token = str.split("\\s+");
        Vector<Integer> sequence = new Vector<Integer>();
        boolean is_valid_query;
        int l = 0;

        for (int i = 0; i < token.length; i++)
        {
            for (int j = 0; j < galRoman.length; j++)
            {
                if (token[i].equals(galRoman[j]))
                {
                    sequence.add(j);
                    l = i;
                }
            }
        }

        if (sequence.size() == 0)
        {
            is_valid_query = false;
        } else
        {
            if (conv.isValidSequence(sequence))
            {
                is_valid_query = true;
                if (!token[l + 1].equals("?"))
                {
                    for (int i = 0; i < metals.size(); i++)
                    {
                        if (!token[l + 1].toLowerCase().equals(metals.get(i)))
                        {
                            is_valid_query = false;
                        } else
                        {
                            is_valid_query = true;
                            i = metals.size();
                        }
                    }
                }
            } else
            {
                is_valid_query = false;
            }
        }

        return is_valid_query;
    }

    private boolean isValidAtr(String str)
    {
        String[] token = str.split("\\s+");
        Vector<Integer> sequence = new Vector<Integer>();
        boolean is_valid_atr;
        int l = 0;

        for (int i = 0; i < token.length; i++)
        {
            for (int j = 0; j < galRoman.length; j++)
            {
                if (token[i].equals(galRoman[j]))
                {
                    sequence.add(j);
                    l = i;
                }
            }
        }

        if (sequence.size() == 0)
        {
            is_valid_atr = false;
        } else
        {
            if (conv.isValidSequence(sequence))
            {
                is_valid_atr = true;
                for (int i = 0; i < token.length; i++)
                {
                    if (token[i].matches("-?\\d+(\\.\\d+)?"))
                    { 
                        for (int j = 0; j < validMetalList.length; j++)
                        {
                            if (token[l + 1].toLowerCase().equals(validMetalList[j]))
                            {
                                is_valid_atr = true;
                                j = validMetalList.length;
                            } else 
                            {
                                is_valid_atr = false;
                            }
                        }
                        i = token.length;
                    }
                }
            } else
            {
                is_valid_atr = false;
            }
        }

        return is_valid_atr;
    }

    private void sortAtr()
    {
        int iter = 0;
        Vector<Integer> aux = new Vector<Integer>();
        String[] temp = new String[atribution.size()];

        for (int i = 0; i < temp.length; i++) {temp[i] = atribution.get(i);}
        atribution.removeAllElements();

        for (int i = 0; i < temp.length; i++)
        {
            String[] token = temp[i].split("\\s+");
            switch (token[token.length - 1])
            {
                case "I":
                    atribution.add(temp[i]);
                    break;
                case "V":
                    atribution.add(temp[i]);
                    break;
                case "X":
                    atribution.add(temp[i]);
                    break;
                case "L":
                    atribution.add(temp[i]);
                    break;
                case "C":
                    atribution.add(temp[i]);
                    break;
                case "D":
                    atribution.add(temp[i]);
                    break;
                case "M":
                    atribution.add(temp[i]);
                    break;
                default:
                    aux.add(i);
                    break;
             }
        }

        for (int i = 0; i < aux.size(); i++) 
        {
            iter = aux.get(i);
            atribution.add(temp[iter]);
        }
    }

    private Vector<Integer> generateSeq(String str)
    {
        String[] token = str.split("\\s+");
        Vector<Integer> sequence = new Vector<Integer>();

        for (int i = 0; i < token.length; i++)
        {
            for (int j = 0; j < galRoman.length; j++)
            {
                if (token[i].equals(galRoman[j]))
                {
                    sequence.add(j);
                    seq += token[i] + " ";
                }
            }
        }

        return sequence;
    }

    private void resetSeqAndResult()
    {
        seq = "";
        result = 0;
    }

    private void createDef()
    {
        double r;
        int l = 0;
        String r_str = "";

        for (int i = 0; i < atribution.size(); i++)
        {
            String[] token = atribution.get(i).split("\\s+");
            switch (token[token.length - 1])
            {
                case "I":
                    galRoman[0] = token[0];
                    break;
                case "V":
                    galRoman[1] = token[0];
                    break;
                case "X":
                    galRoman[2] = token[0];
                    break;
                case "L":
                    galRoman[3] = token[0];
                    break;
                case "C":
                    galRoman[4] = token[0];
                    break;
                case "D":
                    galRoman[5] = token[0];
                    break;
                case "M":
                    galRoman[6] = token[0];
                    break;
                default:
                    if (isValidAtr(atribution.get(i)))
                    {
                        for (int j = 0; j < token.length; j++)
                        {
                            for (int k = 0; k < validMetalList.length; k++)
                            {
                                if (token[j].toLowerCase().equals(validMetalList[k]))
                                {
                                    l = k;
                                    j = token.length;
                                    k = validMetalList.length;
                                }
                            }
                        }

                        for (int j = 0; j < token.length; j++)
                        {
                            if (token[j].matches("-?\\d+(\\.\\d+)?"))
                            {
                                r = Double.parseDouble(token[j]);
                                r = r/conv.evalSequence(generateSeq(atribution.get(i)));
                                r_str += r;
                                metals.add(validMetalList[l]);
                                rates.add(r_str);
                                j = token.length;
                            }
                            r_str = "";
                        }
                     }
                    break;
            }
        }
    }

    public void evalQuery()
    {
        double r;

        resetSeqAndResult();

        for (int i = 0; i < queries.size(); i++)
        {
            if (isValidQuery(queries.get(i)))
            {
                result = conv.evalSequence(generateSeq(queries.get(i)));
                String[] token = queries.get(i).split("\\s+");
                for (int j = 0; j < token.length; j++)
                {
                    for (int k = 0; k < metals.size(); k++)
                    {
                        if (token[j].toLowerCase().equals(metals.get(k)))
                        {
                            r = Double.parseDouble(rates.get(k));
                            result *= r;
                            seq += metals.get(k) + " ";
                        }
                    }
                }
                System.out.println(seq + "is " + result);
                answers.add(seq + " is " + result);

                resetSeqAndResult();
            } else
            {
                System.out.println("I have no idea what you are talking about");
                answers.add("I have no idea what you are talking about");
            }
        }
    }
}
